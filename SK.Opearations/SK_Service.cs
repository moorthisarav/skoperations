﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Common.Logger;
using SK.Library;
using SK.Operations.Api;

namespace SK.Opearations
{
    public partial class SK_Service : ServiceBase
    {
        private WebServiceHost webHost = null; 
        public SK_Service()
        {
            InitializeComponent();
        }

        [Conditional("DEBUG_SERVICE")]
        private static void DebugMode()
        {
            Debugger.Break();
        }

        protected override void OnStart(string[] args)
        {
            #if DEBUG
                DebugMode();
            #endif
            
            string errmsg = "";

            if (!InitEnterpriseLibrary(ref errmsg))
            {
                string msg = "Error initializing Enterprise Library: " + errmsg;
                throw new Exception(msg);
            }

            LoggerHelper.LogTrace("Application_Start", "Application started");

            StartWebService();
        }

        protected override void OnStop()
        {
            if (webHost != null)
            {
                webHost.Close();
                webHost = null;
            }
        }

        private void StartWebService()
        {
            try
            {
                if (webHost != null)
                {
                    webHost.Close();
                }

                string address = CommonConfig.BaseAddress;
                Uri baseAddress = new Uri(address);
                webHost = new WebServiceHost(typeof(OperationsResource), baseAddress);
                webHost.Open();

                LoggerHelper.LogTrace("StartWebService", "web service Application started ->" + address);
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("StartWebService", exp.Message, exp, null);
            }
        }        

        /// <summary>
        /// Initializes the Enterpise Library Application Block.
        /// </summary>
        /// <param name="errmsg">
        /// String containing an error message if an error occurred.
        /// </param>
        /// <returns>
        /// true if successful; otherwise, false.
        /// </returns>
        private bool InitEnterpriseLibrary(ref string errmsg)
        {
            try
            {
                string elconfig = "ELConfig.config";
                string installedDirectory = GetInstalledDirectory();

                string loggingConfigFilePathname = Path.Combine(installedDirectory, elconfig);

                if (!File.Exists(loggingConfigFilePathname))
                {
                    throw new Exception(string.Format("Can't find file: {0} in: {1}",
                                            elconfig,
                                            installedDirectory));
                }

                if (!LoggerHelper.InitEnterpriseLibrary(
                                loggingConfigFilePathname, ref errmsg))
                {
                    throw new Exception(errmsg);
                }

                LoggerHelper.UseSeparateThreadForLogging = true;
                LoggerHelper.DoTraceLogging = true;
                LoggerHelper.PrepareLoggingFlatFiles(loggingConfigFilePathname, "log_archive");

                return true;
            }
            catch (Exception e)
            {
                errmsg = e.Message;
                return false;
            }
        }

        private string GetInstalledDirectory()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();

            string[] nodes = executingAssembly.CodeBase.Split(
                new string[] { "///" }, StringSplitOptions.None);

            string directory = Path.GetDirectoryName(nodes[1].Replace('/', '\\'));

            DirectoryInfo info = Directory.GetParent(directory);

            if (info.Name == "bin")
                info = Directory.GetParent(info.FullName);

            return info.FullName;
        }
    }
}
