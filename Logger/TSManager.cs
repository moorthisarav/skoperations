using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;

namespace Common.Logger
{
    public static class TSManager
    {
        /// <summary>
        /// WTSOpenServer Opens the handle to Server
        /// </summary>
        /// <param name="pServerName"></param>
        /// <returns></returns>
        [DllImport("wtsapi32.dll")]
        static extern IntPtr WTSOpenServer([MarshalAs(UnmanagedType.LPStr)] String pServerName);

        /// <summary>
        /// WTSCloseServer, close the handle to server
        /// </summary>
        /// <param name="hServer"></param>
        [DllImport("wtsapi32.dll")]
        static extern void WTSCloseServer(IntPtr hServer);

        /// <summary>
        /// WTSEnumerateSessions, Enumerate Sessions on a Server
        /// </summary>
        /// <param name="hServer"></param>
        /// <param name="Reserved"></param>
        /// <param name="Version"></param>
        /// <param name="ppSessionInfo"></param>
        /// <param name="pCount"></param>
        /// <returns></returns>
        [DllImport("wtsapi32.dll")]
        static extern Int32 WTSEnumerateSessions(
            IntPtr hServer,
            [MarshalAs(UnmanagedType.U4)] Int32 Reserved,
            [MarshalAs(UnmanagedType.U4)] Int32 Version,
            ref IntPtr ppSessionInfo,
            [MarshalAs(UnmanagedType.U4)] ref Int32 pCount);

        /// <summary>
        /// Structure holding TS Session
        /// </summary>
        public struct TsSession
        {
            public int SessionID;
            public string StationName;
            public string ConnectionState;
            public string UserName;
            public string DomainName;
            public string ClientName;
            public string IpAddress;
        }

        public const int WTS_CURRENT_SESSION = -1;


        /// <summary>
        /// WTSQuerySessionInformation - Query Session Information
        /// </summary>
        /// <param name="hServer"></param>
        /// <param name="sessionId"></param>
        /// <param name="wtsInfoClass"></param>
        /// <param name="ppBuffer"></param>
        /// <param name="pBytesReturned"></param>
        /// <returns></returns>
        [DllImport("Wtsapi32.dll")]
        public static extern bool WTSQuerySessionInformation(
            System.IntPtr hServer, int sessionId, WTS_INFO_CLASS wtsInfoClass, out System.IntPtr ppBuffer, out uint pBytesReturned);

        /// <summary>
        /// Free WTS Memory
        /// </summary>
        /// <param name="pMemory"></param>
        [DllImport("wtsapi32.dll")]
        static extern void WTSFreeMemory(IntPtr pMemory);

        //Structure for TS Client IP Address 
        [StructLayout(LayoutKind.Sequential)]
        private struct _WTS_CLIENT_ADDRESS
        {
            public int AddressFamily;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public byte[] Address;
        }

        /// <summary>
        /// Session Info
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public Int32 SessionID;

            [MarshalAs(UnmanagedType.LPStr)]
            public String pWinStationName;

            public WTS_CONNECTSTATE_CLASS State;
        }

        //Structure for TS Client Information
        public struct WtsClientInfo
        {
            public uint ProcessId;
            public uint ActiveSessionId;
            public bool WTSStatus;
            public string WTSUserName;
            public string WTSStationName;
            public string WTSDomainName;
            public string WTSClientName;
            public int AddressFamily;
            public string Address;
        }

        #region Enumurations

        public enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        public enum WTS_INFO_CLASS
        {
            WTSInitialProgram,
            WTSApplicationName,
            WTSWorkingDirectory,
            WTSOEMId,
            WTSSessionId,
            WTSUserName,
            WTSWinStationName,
            WTSDomainName,
            WTSConnectState,
            WTSClientBuildNumber,
            WTSClientName,
            WTSClientDirectory,
            WTSClientProductId,
            WTSClientHardwareId,
            WTSClientAddress,
            WTSClientDisplay,
            WTSClientProtocolType
        }

        [DllImport("kernel32.dll")]
        static extern uint GetCurrentProcessId();

        [DllImport("kernel32.dll")]
        static extern bool ProcessIdToSessionId(uint dwProcessId, out uint pSessionId);

        /// <summary>
        /// The WTSGetActiveConsoleSessionId function retrieves the 
        /// Terminal Services session currently attached to the physical console. 
        /// The physical console is the monitor, keyboard, and mouse.
        /// </summary>
        /// <returns>An <see cref="int"/> equal to 0 indicates that the current session is attached to the physical console.</returns>
        /// <remarks>It is not necessary that Terminal Services be running for this function to succeed.
        [DllImport("kernel32.dll")]
        static extern int WTSGetActiveConsoleSessionId();

        #endregion

        #region Private Static Variables
        
        private static WtsClientInfo clientInfo = new WtsClientInfo();
        private static bool infoFound = false;

        #endregion

        /// <summary>
        /// Wrapper to Open a Server
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static IntPtr OpenServer(String Name)
        {
            IntPtr server = WTSOpenServer(Name);
            return server;
        }

        /// <summary>
        /// Wrapper to Close Server
        /// </summary>
        /// <param name="ServerHandle"></param>
        public static void CloseServer(IntPtr ServerHandle)
        {
            WTSCloseServer(ServerHandle);
        }

        /// <summary>
        /// Get a List of all Sessions on a server and IP address.
        /// </summary>
        /// <param name="ServerName">Terminal Server Name</param>
        /// <returns></returns>
        public static List<String> ListSessions(String ServerName)
        {
            IntPtr server = IntPtr.Zero;
            List<String> ret = new List<string>();
            server = OpenServer(ServerName);

            try
            {
                IntPtr ppSessionInfo = IntPtr.Zero;

                Int32 count = 0;
                Int32 retval = WTSEnumerateSessions(server, 0, 1, ref ppSessionInfo, ref count);
                Int32 dataSize = Marshal.SizeOf(typeof(WTS_SESSION_INFO));

                Int32 current = (int)ppSessionInfo;

                if (retval != 0)
                {

                    for (int i = 0; i < count; i++)
                    {

                        WTS_SESSION_INFO si = (WTS_SESSION_INFO)Marshal.PtrToStructure((System.IntPtr)current, typeof(WTS_SESSION_INFO));
                        current += dataSize;

                        #region OTsSession
                        uint returned = 0; ;
                        TsSession oTsSession = new TsSession();
                        //IP address 
                        IntPtr addr = IntPtr.Zero;
                        if (WTSQuerySessionInformation(server, si.SessionID, WTS_INFO_CLASS.WTSClientAddress, out addr, out returned) == true)
                        {
                            _WTS_CLIENT_ADDRESS obj = new _WTS_CLIENT_ADDRESS();
                            obj = (_WTS_CLIENT_ADDRESS)Marshal.PtrToStructure(addr, obj.GetType());
                            oTsSession.IpAddress = obj.Address[2] + "." + obj.Address[3] + "." + obj.Address[4] + "." + obj.Address[5];
                        }

                        #endregion

                        ret.Add(si.SessionID + " " + si.State + " " + si.pWinStationName + "  " + oTsSession.IpAddress);

                    }

                    WTSFreeMemory(ppSessionInfo);
                }
            }
            finally
            {
                CloseServer(server);
            }

            return ret;
        }
       
        /// <summary>
        /// Returns the information of the current client session
        /// </summary>
        /// <returns>Client information</returns>
        public static WtsClientInfo GetClientInfo()
        {
            // if this has already been queried
            // return the previously queried value
            if (infoFound)
                return clientInfo;

            string serverName = System.Environment.MachineName;

            //Get ProcessID of TS Session that executed this TS Session
            uint active_process = GetCurrentProcessId();
            uint active_session = 0;
            bool success = ProcessIdToSessionId(active_process, out active_session);
            uint returned;
            IntPtr server = IntPtr.Zero;
            IntPtr wtsValue = IntPtr.Zero;

            clientInfo.WTSStationName = "";
            clientInfo.WTSClientName = "";
            clientInfo.Address = "";

            clientInfo.ProcessId = active_process;
            try
            {
                if (success)
                {
                    clientInfo.ActiveSessionId = active_session;

                    server = OpenServer(serverName);

                    if (server == IntPtr.Zero)
                    {
                        // terminal srver is not running on this machine
                        // user should have logged in locally
                        // get info fromlocal machine
                        PopulateClientInfoForLocalStation();
                    }
                    else
                    {
                        //Get User Name of this TS session
                        if (WTSQuerySessionInformation(server, (int)active_session, WTS_INFO_CLASS.WTSUserName, out wtsValue, out returned))
                        {
                            clientInfo.WTSUserName = Marshal.PtrToStringAnsi(wtsValue);
                            WTSFreeMemory(wtsValue);
                        }

                        //Get StationName of this TS session
                        if (WTSQuerySessionInformation(server, (int)active_session, WTS_INFO_CLASS.WTSWinStationName, out wtsValue, out returned))
                        {
                            clientInfo.WTSStationName = Marshal.PtrToStringAnsi(wtsValue);
                            WTSFreeMemory(wtsValue);
                        }

                        //Get Domain Name of this TS session
                        if (WTSQuerySessionInformation(server, (int)active_session, WTS_INFO_CLASS.WTSDomainName, out wtsValue, out returned))
                        {
                            clientInfo.WTSDomainName = Marshal.PtrToStringAnsi(wtsValue);
                            WTSFreeMemory(wtsValue);
                        }

                        //Skip client name and client address if this is a console session
                        if (clientInfo.WTSStationName != "Console")
                        {
                            if (WTSQuerySessionInformation(server, (int)active_session, WTS_INFO_CLASS.WTSClientName, out wtsValue, out returned))
                            {
                                clientInfo.WTSClientName = Marshal.PtrToStringAnsi(wtsValue);
                                WTSFreeMemory(wtsValue);
                            }

                            // if client name is still null set it to station name
                            if (string.IsNullOrEmpty(clientInfo.WTSClientName))
                            {
                                if (!string.IsNullOrEmpty(clientInfo.WTSStationName))
                                {
                                    clientInfo.WTSClientName = clientInfo.WTSStationName;
                                }
                            }
                            //Get client IP address
                            IntPtr addr;
                            if (WTSQuerySessionInformation(server, (int)active_session, WTS_INFO_CLASS.WTSClientAddress, out addr, out returned))
                            {
                                _WTS_CLIENT_ADDRESS obj = new _WTS_CLIENT_ADDRESS();
                                obj = (_WTS_CLIENT_ADDRESS)Marshal.PtrToStructure(addr, obj.GetType());
                                clientInfo.Address = obj.Address[2] + "." +
                                    obj.Address[3] + "." + obj.Address[4] + "." +
                                    obj.Address[5];
                                WTSFreeMemory(addr);
                            }
                        }
                        else
                        {
                            clientInfo.WTSClientName = System.Environment.MachineName;
                            SetClientAddressAndFamily();
                        }
                    } // else if (server == IntPtr.Zero)
                    infoFound = !string.IsNullOrEmpty(clientInfo.WTSStationName);
                } // if (success)
                else
                {
                    throw new Exception("ProcessIdToSessionId failed");
                }
            }
            finally
            {
                if (server != IntPtr.Zero)
                    WTSCloseServer(server);
            }
            return clientInfo;
        }

        private static void PopulateClientInfoForLocalStation()
        {
            clientInfo.WTSUserName = Environment.UserName;
            clientInfo.WTSStationName = Environment.MachineName;
            clientInfo.WTSClientName = Environment.MachineName;
            clientInfo.WTSDomainName = Environment.UserDomainName;
            SetClientAddressAndFamily();
        }

        private static void SetClientAddressAndFamily()
        {
            try
            {
                clientInfo.Address = Dns.GetHostEntry(Environment.MachineName).AddressList[0].ToString();
                clientInfo.AddressFamily = (int)(Dns.GetHostEntry(Environment.MachineName).AddressList[0].AddressFamily);

            }
            catch (Exception)
            {
                // ignore - we let the client use the name to get the address from dns
            }
        }
    }
}

