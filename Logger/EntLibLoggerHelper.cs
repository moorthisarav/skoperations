﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.Services;       //System.Web.Services.Protocols.SoapException
using System.Windows.Forms;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation; //ManagedSecurityContextInformationProvider

namespace Common.Logger
{
    /// <summary>
    /// This class is a helper class for logging operations.
    /// </summary>
    public static class LoggerHelper
    {
        #region Constants
        private const string DEFAULT_CONFIG_SECTION_NAME_LOGGING = "loggingConfiguration";
        #endregion

        #region InitEnterpriseLibrary

        /// <summary>
        /// Call this method to initial the Enterprise logging library
        /// </summary>
        /// <param name="elconfig"></param>
        /// <param name="errmsg"></param>
        /// <returns></returns>
        public static bool InitEnterpriseLibrary(string elconfig, ref string errmsg)
        {
            try
            {
                if (!File.Exists(elconfig))
                {
                    throw new Exception(String.Format("Can't find file: {0}",
                                            elconfig,
                                            Directory.GetCurrentDirectory()));
                }
                _loggingConfigFilePathname = elconfig;

                //force the getter to create the logwriter
                LogWriter lw = LocalLogWriter;

                return true;
            }
            catch (Exception e)
            {
                errmsg = e.Message;
                return false;
            }
        }


        #endregion

        #region Properties

        /// <summary>
        /// Whether to perform Trace logging
        /// </summary>
        public static bool DoTraceLogging
        {
            get { return _doTraceLogging; }
            set { _doTraceLogging = value; }
        }
        private static bool _doTraceLogging = true;

        /// <summary>
        /// Whether to perform Performance logging
        /// </summary>
        public static bool DoPerformanceLogging
        {
            get { return _doPerformanceLogging; }
            set { _doPerformanceLogging = value; }
        }
        private static bool _doPerformanceLogging = true;

        /// <summary>
        /// Whether to perform Trace Entering/Leaving Method logging
        /// </summary>
        public static bool DoTraceEnteringLeavingMethodLogging
        {
            get { return _doTraceEnteringLeavingMethodLogging; }
            set { _doTraceEnteringLeavingMethodLogging = value; }
        }
        private static bool _doTraceEnteringLeavingMethodLogging = true;

        /// <summary>
        /// Inform LoggerHelper of the filename of the application's app.config file.  
        /// Accepts a relative or an absolute path, such as "appname.exe.config".
        /// </summary>
        public static string LoggingConfigFilePathname
        {
            get { return _loggingConfigFilePathname; }
            set { _loggingConfigFilePathname = value; }
        }
        private static string _loggingConfigFilePathname = null;

        public static LogWriter LocalLogWriter
        {
            get
            {
                if (_localLogWriter == null)
                {
                    // if no logging file pathname was given, default to the application or web config
                    // file
                    IConfigurationSource configSource = null;
                    if (String.IsNullOrEmpty(LoggingConfigFilePathname))
                    {
                        configSource = new SystemConfigurationSource();
                    }
                    else
                    {
                        // use the file pathname that was specified
                        configSource = new FileConfigurationSource(LoggingConfigFilePathname);
                    }

                    // use a LogWriterFactory to create a LogWriter
                    LogWriterFactory logWriterFactory = new LogWriterFactory(configSource);
                    _localLogWriter = logWriterFactory.Create();
                }
                return _localLogWriter;
            }

        }
        private static LogWriter _localLogWriter = null;

        /// <summary>
        /// Inform LoggerHelper of the section name within App.config for Enterprise Liabrary logging configuration.
        /// </summary>
        public static string LoggingConfigSectionName
        {
            get { return LoggerHelper._LoggingConfigSectionName; }
            set { LoggerHelper._LoggingConfigSectionName = value; }
        }
        private static string _LoggingConfigSectionName = DEFAULT_CONFIG_SECTION_NAME_LOGGING;

        /// <summary>
        /// Use a separate thread for logging. Note that this will cause a "junk" log file 
        /// with a GUID filename to be created by Enterprise Library in the new thread's appDomain.
        /// </summary>
        public static bool UseSeparateThreadForLogging
        {
            get { return LoggerHelper._useSeparateThreadForLogging; }
            set { LoggerHelper._useSeparateThreadForLogging = value; }
        }
        private static bool _useSeparateThreadForLogging = true;

        #endregion

        #region logTH
        static Thread logTh = null;
        #endregion

        #region Constructor

        /// <summary>
        /// The static ctor inits the logQueue and the log thread
        /// </summary>
        static LoggerHelper()
        {
            try
            {
                syncobj = new object();
                logQueue = new Queue<LogEntry>();

                //*** jsm, 4/2/07 - I thought this was taken care of previously, but it seems to have gotton lost
                Application.ApplicationExit += new EventHandler(Application_ApplicationExit);

                logTh = new Thread(new ThreadStart(WriteLogEntryThreadProcEx));
                logTh.Name = "Log Helper Thread";
                logTh.Start();
            }
            catch (Exception e)
            {

            }

        }
        #endregion

        #region Application_ApplicationExit

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            try
            {
                if (logTh != null &&
                    (logTh.ThreadState == System.Threading.ThreadState.Running ||
                     logTh.ThreadState == System.Threading.ThreadState.WaitSleepJoin ||
                     logTh.ThreadState == System.Threading.ThreadState.Background))
                {
                    if (logTh.ThreadState == System.Threading.ThreadState.WaitSleepJoin)
                    {
                        logTh.Interrupt();
                    }
                    logTh.Abort();
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Logging Methods

        /// <summary>
        /// Private function that performs actual writing of the log entry.  
        /// </summary>
        /// <param name="logEntry"></param>
        private static void WriteLogEntry(LogEntry logEntry)
        {
            if (UseSeparateThreadForLogging)
            {
                //*** jsm, 8/29/06
                //*** I believe the information about AppDomain and the singleton instance in the
                //*** Enterprise library (found at the URLs below) is incorrect. The AppDomain
                //*** information at the shawnfa blog is correct, however it has nothing to do
                //*** with the logging in the Enterprise library. I believe the conclusion reached
                //*** at the www.gotdotnet url is incorrect. Why? Unless you write code to add
                //*** another AppDomain in your process - there will only be one AppDomain. Basically
                //*** an AppDomain is a "logical" .NET process that is hosted in an operating
                //*** system process. The startup code generated by
                //*** VS.NET will make the calls needed to host the .NET framework in your process -
                //*** and it will load exactly one AppDomain. I did a search of the Enterprise
                //*** logging code and they do not appear to create another AppDomain.
                //
                //NOTE: If UseSeparateThreadForLogging = true, this will cause a "junk" log file 
                //with a GUID filename to be created by Enterprise Library in the new thread's appDomain.
                //Info on how to fix this:
                //http://www.gotdotnet.com/Community/MessageBoard/Thread.aspx?id=372718
                //and
                //"I resolved this by using the suggestion for maintaining a singleton across appdomains found 
                //at http://blogs.msdn.com/shawnfa/archive/2005/08/22/454794.aspx"

                // make call to the EL logger in a background thread so that user activities are not
                // delayed by the logging
#if USE_SINGLE_LOG_THREAD
                WriteLogEntryEx(logEntry);
#else
                Thread loggingThread = new Thread(new ParameterizedThreadStart(WriteLogEntryThreadProc));
                loggingThread.Name = "Logging Thread";
                loggingThread.Start(logEntry);
#endif
            }
            else
            {
                LocalLogWriter.Write(logEntry);
            }
        }

        /// <summary>
        /// Thread Proc for writing an entry to the logger
        /// </summary>
        /// <param name="logEntryObj"></param>
        private static void WriteLogEntryThreadProc(object logEntryObj)
        {
            // put try/catch around logic to ensure that we don't generate an unhandled exception
            try
            {
                LogEntry logEntry = logEntryObj as LogEntry;

                if (logEntry != null)
                {
                    // synchronize on the LocalLogWriter to ensure we don't have cross-thread contention
                    lock (LocalLogWriter)
                    {
                        LocalLogWriter.Write(logEntry);
                    }
                }
            }
            catch
            {
                // merely eat the exception -- we don't want to request to log the error as this
                // could create an infinite loop of logging calls
            }
        }

        #region WriteLogEntryEx, WriteLogEntryThreadProcEx
        //*** jsm, 8/26/06
        //added single logging thread using Queue<LogEntry>, with basic Monitor synchronization

        /// <summary>
        /// Private function that performs actual writing of the log entry.  
        /// </summary>
        /// <param name="logEntry"></param>
        private static void WriteLogEntryEx(LogEntry logEntry)
        {
            try
            {
                lock (syncobj)
                {
                    logQueue.Enqueue(logEntry);
                }
            }
            catch
            {


            }
        }

        //logQueue and syncobj are instantiated in the static ctor
        private static Queue<LogEntry> logQueue = null;
        private static object syncobj = null;
        const int SleepTime = 50;
        /// <summary>
        /// Thread Proc for writing an entry to the logger
        /// </summary>        ///
        private static void WriteLogEntryThreadProcEx()
        {
            // put try/catch around logic to ensure that we don't generate an unhandled exception
            try
            {
                LogEntry logEntry = null;
                while (true)
                {
                    logEntry = null;
                    //use TryEnter, if we can not get it this time, we will get it on the next loop
                    if (Monitor.TryEnter(syncobj, 0))
                    {
                        if (logQueue.Count > 0)
                        {
                            logEntry = logQueue.Dequeue();
                        }
                        Monitor.Exit(syncobj);
                    }

                    if (logEntry != null)
                    {
                        //Since we only have 1 log thread now, don't need to lock on LocalLogWriter
                        LocalLogWriter.Write(logEntry);
                    }
                    Thread.Sleep(SleepTime);
                }
            }
            catch
            {
                // merely eat the exception -- we don't want to request to log the error as this
                // could create an infinite loop of logging calls
            }
        }

        #endregion
        /// <summary>
        /// Logs a critical error
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void LogCriticalError(string title, string message)
        {
            Exception ex = null;
            LogCriticalError(title, message, ex, null);
        }

        /// <summary>
        /// Logs a critical error.  Inner exceptions will be expanded into the message.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ex">System.Exception that was caught.  Inner exceptions will be expanded into the message.</param>
        /// <param name="contextInfo">
        ///   Dictionary of context information to be recorded with the log message,
        ///   such as key values that encountered an error.
        ///  <para>
        ///   Dictionary &lt;string, object&gt; contextInfo = new Dictionary&lt;string, object&gt;(); 
        ///   contextInfo.Add("Context key name", "Context key value");</para></param>
        public static void LogCriticalError(string title, string message, Exception ex, Dictionary<string, object> contextInfo)
        {
            if (ex != null)
            {
                message += FormatExceptionMessageWithInnerExceptions(ex);
            }
            LogEntry logEntry = new LogEntry();
            logEntry.Categories.Add("Critical Error");
            logEntry.Title = "*** CRITICAL ERROR *** - " + title;
            logEntry.Severity = System.Diagnostics.TraceEventType.Critical;
            logEntry.Priority = 1;
            logEntry.TimeStamp = DateTime.Now;
            logEntry.Message = message;

            //Add context information collection
            if (contextInfo == null)
            {
                contextInfo = new Dictionary<string, object>();
            }
            //Extra standard process info
            //AuthenticationType, IdentityName, IsAuthenticated 
            ManagedSecurityContextInformationProvider informationHelper =
                    new ManagedSecurityContextInformationProvider();
            informationHelper.PopulateDictionary(contextInfo);
            logEntry.ExtendedProperties = contextInfo;

            WriteLogEntry(logEntry);
        }//LogCriticalError

        /// <summary>
        /// Logs a non-critical error
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void LogError(string title, string message)
        {
            LogError(title, message, null, null);
        }

        /// <summary>
        /// Logs a non-critical error
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ex">System.Exception that was caught.  Inner exceptions will be expanded into the message.</param>
        /// <param name="contextInfo">
        ///   Dictionary of context information to be recorded with the log message,
        ///   such as key values that encountered an error.
        ///  <para>
        ///   Dictionary &lt;string, object&gt; contextInfo = new Dictionary&lt;string, object&gt;(); 
        ///   contextInfo.Add("Context key name", "Context key value");</para></param>
        public static void LogError(string title, string message, Exception ex, Dictionary<string, object> contextInfo)
        {
            if (ex != null)
            {
                message += FormatExceptionMessageWithInnerExceptions(ex);
            }
            LogEntry logEntry = new LogEntry();
            logEntry.Categories.Add("Error");
            logEntry.Title = "*** ERROR *** - " + title;
            logEntry.Severity = System.Diagnostics.TraceEventType.Error;
            logEntry.Priority = 2;
            logEntry.TimeStamp = DateTime.Now;
            logEntry.Message = message;            
            //Add context information collection
            if (contextInfo == null)
            {
                contextInfo = new Dictionary<string, object>();
            }



            //Extra standard process info
            //AuthenticationType, IdentityName, IsAuthenticated 
            ManagedSecurityContextInformationProvider informationHelper =
                    new ManagedSecurityContextInformationProvider();
            informationHelper.PopulateDictionary(contextInfo);
            logEntry.ExtendedProperties = contextInfo;

            WriteLogEntry(logEntry);
        }//LogError

        /// <summary>
        /// Logs a warning
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void LogWarning(string title, string message)
        {
            LogWarning(title, message, null, null);
        }

        /// <summary>
        /// Logs a warning
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ex">System.Exception that was caught.  Inner exceptions will be expanded into the message.</param>
        /// <param name="contextInfo">
        ///   Dictionary of context information to be recorded with the log message,
        ///   such as key values that encountered an error.
        ///  <para>
        ///   Dictionary &lt;string, object&gt; contextInfo = new Dictionary&lt;string, object&gt;(); 
        ///   contextInfo.Add("Context key name", "Context key value")</para></param>
        public static void LogWarning(string title, string message, Exception ex, Dictionary<string, object> contextInfo)
        {
            if (ex != null)
            {
                message += FormatExceptionMessageWithInnerExceptions(ex);
            }
            LogEntry logEntry = new LogEntry();
            logEntry.Categories.Add("Warning");
            logEntry.Title = "*** Warning *** - " + title;
            logEntry.Severity = System.Diagnostics.TraceEventType.Warning;
            logEntry.Priority = 3;
            logEntry.TimeStamp = DateTime.Now;
            logEntry.Message = message;
            //Add context information collection
            if (contextInfo == null)
            {
                contextInfo = new Dictionary<string, object>();
            }
            //Extra standard process info
            //AuthenticationType, IdentityName, IsAuthenticated 
            ManagedSecurityContextInformationProvider informationHelper =
                    new ManagedSecurityContextInformationProvider();
            informationHelper.PopulateDictionary(contextInfo);
            logEntry.ExtendedProperties = contextInfo;

            WriteLogEntry(logEntry);
        }//LogWarning

        /// <summary>
        /// Logs a trace message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void LogTrace(string title, string message)
        {
            LogTrace(title, message, null, null);
        }

        /// <summary>
        /// Logs a trace message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ex">System.Exception that was caught.  Inner exceptions will be expanded into the message.</param>
        /// <param name="contextInfo">
        ///   Dictionary of context information to be recorded with the log message,
        ///   such as key values that encountered an error.
        ///  <para>
        ///   Dictionary &lt;string, object&gt; contextInfo = new Dictionary&lt;string, object&gt;(); 
        ///   contextInfo.Add("Context key name", "Context key value");</para></param>
        public static void LogTrace(string title, string message, Exception ex, Dictionary<string, object> contextInfo)
        {
            if (ex != null)
            {
                message += FormatExceptionMessageWithInnerExceptions(ex);
            }
            if (DoTraceLogging)
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                logEntry.Title = title;
                logEntry.Severity = System.Diagnostics.TraceEventType.Information;
                logEntry.Priority = 4;
                logEntry.TimeStamp = DateTime.Now;
                logEntry.Message = message;

                //Add context information collection
                if (contextInfo == null)
                {
                    contextInfo = new Dictionary<string, object>();
                }
                //Extra standard process info
                //AuthenticationType, IdentityName, IsAuthenticated 
                ManagedSecurityContextInformationProvider informationHelper =
                        new ManagedSecurityContextInformationProvider();
                informationHelper.PopulateDictionary(contextInfo);
                logEntry.ExtendedProperties = contextInfo;

                WriteLogEntry(logEntry);
            }
        }//LogTrace

        /// <summary>
        /// Logs a Performance Trace message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void LogTracePerformance(string title, string message)
        {
            LogTracePerformance(title, message, null, null);
        }

        /// <summary>
        /// Logs a Performance Trace message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ex">System.Exception that was caught.  Inner exceptions will be expanded into the message.</param>
        /// <param name="contextInfo">
        ///   Dictionary of context information to be recorded with the log message, such as key values that encountered an error.
        ///   Dictionary<string, object> contextInfo = new Dictionary<string, object>();
        ///   contextInfo.Add("Context key name", "Context key value");
        /// </param>
        public static void LogTracePerformance(string title, string message, Exception ex, Dictionary<string, object> contextInfo)
        {
            if (ex != null)
            {
                message += FormatExceptionMessageWithInnerExceptions(ex);
            }
            if (DoPerformanceLogging)
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace - Performance");
                logEntry.Title = title;
                logEntry.Severity = System.Diagnostics.TraceEventType.Information;
                logEntry.Priority = 4;
                logEntry.TimeStamp = DateTime.Now;
                logEntry.Message = message;
                //Add context information collection
                if (contextInfo == null)
                {
                    contextInfo = new Dictionary<string, object>();
                }
                //Extra standard process info
                //AuthenticationType, IdentityName, IsAuthenticated 
                ManagedSecurityContextInformationProvider informationHelper =
                        new ManagedSecurityContextInformationProvider();
                informationHelper.PopulateDictionary(contextInfo);
                logEntry.ExtendedProperties = contextInfo;

                WriteLogEntry(logEntry);
            }
        }//LogTracePerformance

        /// <summary>
        /// Logs a trace message with information associated with entering a method
        /// </summary>
        /// <param name="fullyQualifiedModuleName"></param>
        /// <param name="typeFullName"></param>
        /// <param name="methodName"></param>
        public static void LogTraceEnteringMethod(string fullyQualifiedModuleName, string typeFullName, string methodName)
        {
            if (DoTraceEnteringLeavingMethodLogging)
            {
                LoggerHelper.LogTrace(" Entering method",
                                      String.Format("Type/Class: {0} \r\nMethod: {1} \r\nModule: {2}",
                                                    typeFullName, methodName, fullyQualifiedModuleName));
            }
        }

        /// <summary>
        /// Logs a trace message with information associated with leaving a method
        /// </summary>
        /// <param name="fullyQualifiedModuleName"></param>
        /// <param name="typeFullName"></param>
        /// <param name="methodName"></param>
        public static void LogTraceLeavingMethod(string fullyQualifiedModuleName, string typeFullName, string methodName)
        {
            if (DoTraceEnteringLeavingMethodLogging)
            {
                LoggerHelper.LogTrace(" Leaving method",
                                      String.Format("Type/Class: {0} \r\nMethod: {1} \r\nModule: {2}",
                                                    typeFullName, methodName, fullyQualifiedModuleName));
            }
        }

        #region LogTraceEx, LogException, LogCriticalException, GetExtraParams
        //*** jsm, 3/25/08 - there are wrapper methods for LogTrace, and LogError 
        //These take a option parameters

        static string GetExtraParams(object[] extraP)
        {
            if (extraP == null) return String.Empty;

            //make sure caller has a try/catch
            StringBuilder sb = new StringBuilder(String.Empty);
            for (int i = 0; i < extraP.Length; ++i)
            {
                sb.AppendFormat("{0} ", (extraP[i] == null ? String.Empty : extraP[i].ToString()));
            }
            return sb.ToString();

        }

        /// <summary>
        /// Log a trace with optional parameters
        /// </summary>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        /// <param name="extraP"></param>
        public static void LogTraceEx(string title, string msg, params object[] extraP)
        {
            try
            {
                StringBuilder sb = new StringBuilder(msg);
                sb.AppendFormat(" {0}", GetExtraParams(extraP));
                LogTrace(title, sb.ToString());
            }
            catch (Exception e0)
            {
                //a backup
                System.Diagnostics.Trace.WriteLine(String.Format("Exception in LogTraceEx:{0}", e0.Message));

            }


        }

        /// <summary>
        /// Log an exception with var args
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="e0"></param>
        /// <param name="extraP"></param>
        public static void LogException(string title, string message, Exception e0, params object[] extraP)
        {
            try
            {
                StringBuilder sb = new StringBuilder(message);
                sb.AppendFormat(" {0}", GetExtraParams(extraP));
                LogError(title, sb.ToString(), e0, null);
            }
            catch (Exception e1)
            {
                //a backup
                System.Diagnostics.Trace.WriteLine(String.Format("Exception in LogException:{0}", e1.Message));
            }

        }

        /// <summary>
        /// Log an exception with var args
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="e0"></param>
        /// <param name="extraP"></param>
        public static void LogCriticalException(string title, string message, Exception e0, params object[] extraP)
        {
            try
            {
                StringBuilder sb = new StringBuilder(message);
                sb.AppendFormat(" {0}", GetExtraParams(extraP));
                LogCriticalError(title, sb.ToString(), e0, null);
            }
            catch (Exception e1)
            {
                //a backup
                System.Diagnostics.Trace.WriteLine(String.Format("Exception in LogCriticalException: {0}", e1.Message));
            }

        }
        #endregion


        #endregion

        #region Formatting Methods

        public static string FormatDBAccessPerformanceTraceMessage(string module, string method, string command, long milliseconds)
        {
            if (DoPerformanceLogging)
            {
                return String.Format(" Response time: {0} milliseconds \r\n Module: {1} \r\n Method: {2} \r\n DB Command: {2}",
                                     milliseconds, module, method, command);
            }
            else
            {
                return "Performance Logging turned off";
            }
        }

        /// <summary>
        /// Extract and format messages from an exception and any inner exceptions.  
        /// Also formats SoapExceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string FormatExceptionMessageWithInnerExceptions(Exception ex)
        {
            string msg = "";
            Exception innerEx = ex;
            while (innerEx != null)
            {
                if (innerEx is System.Web.Services.Protocols.SoapException)
                {
                    string formattedSoapMsg = innerEx.Message;
                    formattedSoapMsg = formattedSoapMsg.Replace("--->", "\r\n--->");
                    formattedSoapMsg = formattedSoapMsg.Replace("SoapException:", "SoapException:\r\n--->");
                    msg += formattedSoapMsg;
                }
                else
                {
                    msg += "\r\n--> " + innerEx.Message;
                }
                innerEx = innerEx.InnerException;
            }
            

            if(! string.IsNullOrEmpty( ex.StackTrace))
                msg += "\r\n--> " + ex.StackTrace;

            return msg;

        }//FormatExceptionMessageWithInnerExceptions

        #endregion

        #region File Preparation Methods

        /// <summary>
        /// Prepares the logging flat files for the current application or web service.
        /// </summary>
        /// <param name="configFilePathname">Full pathname of the file that contains Logging configuration info</param>
        /// <param name="archiveDir">Directory location for archive log files</param>
        /// <remarks>
        /// The Microsoft Logging Application Block (LAB) provides an ability to log information to 
        /// flat files.  However, the LAB requires that the flat file names be named specifically in
        /// the configuration for the LAB.  This introduces an issue whereby a flat file used for 
        /// tracing will be ever expanded in size with each passing day.
        /// 
        /// We want to have daily log files, primarily for trace logging, but
        /// log files may be used for other purposes as well.  While it is possible to extend the LAB
        /// to create a custom FlatFileTraceListener that automatically sets up daily trace filenames,
        /// it is just as effective, and requires less development and maintenance effort, to 
        /// use the single "standard" filename specification provided by the LAB configuration and keep daily
        /// logs by renaming the "standard" filename whenever desired.
        /// 
        /// This method handles that work for the application. 
        /// 
        /// Notes:        
        /// <list>
        /// <item>If the specified configuration file does not contain "logging" configuration, this
        /// method will not take any action with the log file.</item>
        /// <item>If the specified configuration file contains "logging" configuration, but does not
        /// have a FlatFileTraceListener in the logging configuration, this
        /// method will not take any action with the log file.</item>
        /// <item>If the trace log file is renamed, another fresh trace log file will be created
        /// for the current day's logging.</item>
        /// </list>
        /// </remarks>
        public static void PrepareLoggingFlatFiles(string configFilePathname, string archiveDir)
        {
            try
            {
                if (String.IsNullOrEmpty(configFilePathname))
                {
                    throw new ArgumentException("The Configuration File Pathname argument cannot be empty or null");
                }

                if (!File.Exists(configFilePathname))
                {
                    throw new ArgumentException("The Configuration File Pathname does not represent a valid file");
                }


                string logFileDirectory = "";
                string logFilenameWithoutDirectory = "";
                string logFilenameWithoutExtension = "";
                string logFileExtension = "";
                string logFilePathname = "";

                //// get the current configuration file information that applies to all users
                //System.Configuration.Configuration config =
                //    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None) as Configuration;
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = configFilePathname;  // note: relative paths will work

                // Open config file 
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                if (config == null)
                {
                    return;
                }

                // get the logging configuration section
                ConfigurationSection loggingConfigSection =
                    config.GetSection(LoggingConfigSectionName) as ConfigurationSection;
                if (loggingConfigSection == null)
                {
                    return;
                }

                // get the listener configuration properties withing the logging configuration section
                PropertyInformation listenersPropertyInfo = loggingConfigSection.ElementInformation.Properties["listeners"];
                TraceListenerDataCollection traceListeners = listenersPropertyInfo.Value as TraceListenerDataCollection;
                if (traceListeners == null)
                {
                    return;
                }

                // Iterate through the TraceListenerData objects looking for each instance of a
                // FlatFileTraceListenerData.  For each instance, take appropriate action with the 
                // specified filename
                foreach (TraceListenerData traceListenerData in traceListeners)
                {
                    // if we found a FlatFileTraceListenerData...
                    if (traceListenerData.ListenerDataType == typeof(FlatFileTraceListenerData))
                    {
                        logFilePathname = ((FlatFileTraceListenerData)traceListenerData).FileName;

                        int idxLastSlashOrBackslash = logFilePathname.LastIndexOfAny(@"/\".ToCharArray());
                        // if no Slash ('/') or Backslash ('\') was found, 
                        if (idxLastSlashOrBackslash == -1)
                        {
                            logFilenameWithoutDirectory = logFilePathname;
                        }
                        else
                        {
                            logFileDirectory = logFilePathname.Substring(0, idxLastSlashOrBackslash + 1);
                            logFilenameWithoutDirectory = logFilePathname.Substring(logFileDirectory.Length, logFilePathname.Length - logFileDirectory.Length);
                        }

                        int idxLastPeriod = logFilenameWithoutDirectory.LastIndexOf('.');
                        if (idxLastPeriod != -1)
                        {
                            logFileExtension = logFilenameWithoutDirectory.Substring(idxLastPeriod);

                            logFilenameWithoutExtension = logFilenameWithoutDirectory.Substring(0, idxLastPeriod);
                        }

                        // if a trace file already exists...
                        if (File.Exists(logFilePathname))
                        {
                            // perform a lock to ensure no other thread can access this body of
                            // code while the current thread is executing 
                            lock (logFilePathname)
                            {
                                // get the creation timestamp for the current file
                                DateTime fileCreationTime = File.GetCreationTime(logFilePathname);

                                // if the file was created on a previous day, make a copy of the trace file with the 
                                // date embedded in the filename and delete the current file -- we're going to create
                                // a fresh new trace file for today
                                if (fileCreationTime.Date < DateTime.Now.Date)
                                {
                                    // create the date extension string from the file's creation time
                                    string dateExtension = fileCreationTime.Year.ToString("0000") +
                                                           fileCreationTime.Month.ToString("00") +
                                                           fileCreationTime.Day.ToString("00");

                                    // prepare the archive directory 
                                    archiveDir = archiveDir.Trim();
                                    if (archiveDir != "")
                                    {
                                        // if the archive directory doesn't end in a backslash ('\'), append one
                                        if (archiveDir[archiveDir.Length - 1] != '\\')
                                        {
                                            archiveDir += '\\';
                                        }

                                        // if the archive directory doesn't exist, create it
                                        if (!Directory.Exists(archiveDir))
                                        {
                                            Directory.CreateDirectory(archiveDir);
                                        }
                                    }

                                    // piece the filename together with date embedded in filename
                                    string newFilename = String.Format("{0}{1}_{2}{3}", archiveDir, logFilenameWithoutExtension, dateExtension, logFileExtension);

                                    // if a file already exists with the new filename, delete it
                                    if (File.Exists(newFilename))
                                    {
                                        File.Delete(newFilename);
                                    }

                                    // copy the current trace file to the new filename
                                    //NOTE: If UseSeparateThreadForLogging = true, this will cause a "junk" file 
                                    //with a GUID filename to be created by Enterprise Library in the new appDomain.
                                    //See notes in property UseSeparateThreadForLogging.
                                    File.Move(logFilePathname, newFilename);
                                }
                            }
                        }
                    }
                }

            }
            catch (ConfigurationErrorsException err)
            {
                Console.WriteLine(err.ToString());
            }
        }

        #endregion

        #region LogEntryToString

        public static string LogEntryToString(LogEntry le)
        {
            return String.Format("{0}: {1}", le.TimeStampString, le.Message);

        }

        #endregion

    }
}
