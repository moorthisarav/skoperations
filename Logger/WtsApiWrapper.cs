using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WtsApiWrapper
{
    public class WtsHelper
    {
        enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct WTS_SESSION_INFO
        {
            public int SessionID; // DWORD integer
            public string pWinStationName; // integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
            public WTS_CONNECTSTATE_CLASS State;
        }

        struct strSessionsInfo
        {
            public int SessionID;
            public string StationName;
            public string ConnectionState;
        }

        public enum WTS_INFO_CLASS
        {
            WTSInitialProgram = 0,
            WTSApplicationName = 1,
            WTSWorkingDirectory = 2,
            WTSOEMId = 3,
            WTSSessionId = 4,
            WTSUserName = 5,
            WTSWinStationName = 6,
            WTSDomainName = 7,
            WTSConnectState = 8,
            WTSClientBuildNumber = 9,
            WTSClientName = 10,
            WTSClientDirectory = 11,
            WTSClientProductId = 12,
            WTSClientHardwareId = 13,
            WTSClientAddress = 14,
            WTSClientDisplay = 15,
            WTSClientProtocolType = 16,
            WTSIdleTime = 17,
            WTSLogonTime = 18,
            WTSIncomingBytes = 19,
            WTSOutgoingBytes = 20,
            WTSIncomingFrames = 21,
            WTSOutgoingFrames = 22
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct WTS_CLIENT_ADDRESS
        {
            public uint AddressFamily;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public byte[] Address;
        }

        //Structure for TS Client Information
        private struct WTS_CLIENT_INFO
        {
            public bool WTSStatus;
            public string WTSUserName;
            public string WTSStationName;
            public string WTSDomainName;
            public string WTSClientName;
            public int AddressFamily;
            public byte[] Address;
        }

        [DllImport("wtsapi32.dll", SetLastError = true)]
        static extern bool WTSEnumerateProcesses(
            IntPtr serverHandle, // Handle to a terminal server. 
            Int32 reserved,     // must be 0
            Int32 version,      // must be 1
            ref IntPtr ppProcessInfo, // pointer to array of WTS_PROCESS_INFO
            ref Int32 pCount     // pointer to number of processes
        );

        [DllImport("kernel32.dll")]
        static extern uint GetCurrentProcessId();

        [DllImport("kernel32.dll")]
        static extern bool ProcessIdToSessionId(uint dwProcessId, out uint pSessionId);

        /// <summary>
        /// The WTSGetActiveConsoleSessionId function retrieves the 
        /// Terminal Services session currently attached to the physical console. 
        /// The physical console is the monitor, keyboard, and mouse.
        /// </summary>
        /// <returns>An <see cref="int"/> equal to 0 indicates that the current session is attached to the physical console.</returns>
        /// <remarks>It is not necessary that Terminal Services be running for this function to succeed.
        /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/termserv/termserv/wtsgetactiveconsolesessionid.asp</remarks>
        [DllImport("kernel32.dll")]
        static extern int WTSGetActiveConsoleSessionId();

        /// <summary>
        /// The WTSQuerySessionInformation function retrieves session information for the specified 
        /// session on the specified terminal server. 
        /// It can be used to query session information on local and remote terminal servers.
        /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/termserv/termserv/wtsquerysessioninformation.asp
        /// </summary>
        /// <param name="hServer">Handle to a terminal server. Specify a handle opened by the WTSOpenServer function, 
        /// or specify <see cref="WTS_CURRENT_SERVER_HANDLE"/> to indicate the terminal server on which your application is running.</param>
        /// <param name="sessionId">A Terminal Services session identifier. To indicate the session in which the calling application is running 
        /// (or the current session) specify <see cref="WTS_CURRENT_SESSION"/>. Only specify <see cref="WTS_CURRENT_SESSION"/> when obtaining session information on the 
        /// local server. If it is specified when querying session information on a remote server, the returned session 
        /// information will be inconsistent. Do not use the returned data in this situation.</param>
        /// <param name="wtsInfoClass">Specifies the type of information to retrieve. This parameter can be one of the values from the <see cref="WTSInfoClass"/> enumeration type. </param>
        /// <param name="ppBuffer">Pointer to a variable that receives a pointer to the requested information. The format and contents of the data depend on the information class specified in the <see cref="WTSInfoClass"/> parameter. 
        /// To free the returned buffer, call the <see cref="WTSFreeMemory"/> function. </param>
        /// <param name="pBytesReturned">Pointer to a variable that receives the size, in bytes, of the data returned in ppBuffer.</param>
        /// <returns>If the function succeeds, the return value is a nonzero value.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.
        /// </returns>
        //[DllImport("Wtsapi32.dll")]
        //public static extern bool WTSQuerySessionInformation(
        //    System.IntPtr hServer, int sessionId, WTS_INFO_CLASS wtsInfoClass, out System.IntPtr ppBuffer, out uint pBytesReturned);

        // <MarshalAs(UnmanagedType.LPWStr)> ByRef ppBuffer As String
        [DllImport("Wtsapi32.dll")]
        public static extern bool WTSQuerySessionInformation(
            System.IntPtr hServer, int sessionId, WTS_INFO_CLASS wtsInfoClass, [MarshalAs(UnmanagedType.LPWStr)] string ppBuffer, out uint pBytesReturned);

        [DllImport("wtsapi32.dll", ExactSpelling = true, SetLastError = false)]
        public static extern void WTSFreeMemory(IntPtr memory);


        [DllImport("wtsapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr WTSOpenServer(string pServerName);

        [DllImport("wtsapi32.dll")]
        static extern void WTSCloseServer(IntPtr hServer);

        [DllImport("wtsapi32.dll")]
        static extern int WTSEnumerateSessions(
                System.IntPtr hServer,
                int Reserved,
                int Version,
                ref System.IntPtr ppSessionInfo,
                ref int pCount);

        //static extern void WTSEnumerateSessions(
        //                System.IntPtr hServer,
        //                ref System.IntPtr ppSessionInfo,
        //                ref int pCount)
        //{
        //    WTSEnumerateSessions(hServer, 0, 1, ppSessionInfo, pCount);
        //}

        bool GetSessions(string ServerName, ref WTS_CLIENT_INFO ClientInfo)
        {
            IntPtr ptrOpenedServer;
            try
            {
                ptrOpenedServer = WTSOpenServer(ServerName);
                if (ptrOpenedServer == null)
                {
                    //MessageBox.Show("Terminal Services not running on : " + ServerName);
                    return false;
                }
                int FRetVal;
                IntPtr ppSessionInfo = IntPtr.Zero;
                int Count = 0;
                try
                {
                    FRetVal = WTSEnumerateSessions(ptrOpenedServer, 0, 1, ref ppSessionInfo, ref Count);
                    if (FRetVal != 0)
                    {
                        WTS_SESSION_INFO[] sessionInfo = new WTS_SESSION_INFO[Count];
                        int i;
                        System.IntPtr session_ptr;
                        for (i = 0; i < Count; i++)
                        {
                            session_ptr = ppSessionInfo.ToInt32() + (i * Marshal.SizeOf(sessionInfo[i]));
                            sessionInfo[i] = (WTS_SESSION_INFO)Marshal.PtrToStructure(session_ptr, typeof(WTS_SESSION_INFO));
                        }
                        WTSFreeMemory(ppSessionInfo);
                        strSessionsInfo[] tmpArr = new strSessionsInfo[sessionInfo.Length];
                        for (i = 0; i < tmpArr.Length; i++)
                        {
                            tmpArr[i].SessionID = sessionInfo[i].SessionID;
                            tmpArr[i].StationName = sessionInfo[i].pWinStationName;
                            tmpArr[i].ConnectionState = GetConnectionState(sessionInfo[i].State);
                            //MessageBox.Show(tmpArr(i).StationName & "  " & tmpArr(i).SessionID & "  " & tmpArr(i).ConnectionState)
                        }
                        sessionInfo = null;
                    }
                    else
                    {
                        throw new Exception("No data retruned");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + Environment.NewLine + System.Runtime.InteropServices.Marshal.GetLastWin32Error());

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //Get ProcessID of TS Session that executed this TS Session
            uint active_process = GetCurrentProcessId();
            uint active_session = 0;
            bool success1 = ProcessIdToSessionId(active_process, out active_session);
            if (success1 == false)
            {
                //MessageBox.Show("Error: ProcessIdToSessionId");
            }
            uint returned;
            string str = "";
            bool success = false;
            ClientInfo.WTSStationName = "";
            ClientInfo.WTSClientName = "";
            ClientInfo.Address[2] = 0;
            ClientInfo.Address[3] = 0;
            ClientInfo.Address[4] = 0;
            ClientInfo.Address[5] = 0;

            //Get User Name of this TS session
            if (WTSQuerySessionInformation(ptrOpenedServer, (int) active_session, WTS_INFO_CLASS.WTSUserName, str, out returned))
            {
                ClientInfo.WTSUserName = str;
            }

            //Get StationName of this TS session
            if (WTSQuerySessionInformation(ptrOpenedServer, (int) active_session, WTS_INFO_CLASS.WTSWinStationName, str, out returned))
            {
                ClientInfo.WTSStationName = str;
            }

            //Get Domain Name of this TS session
            if (WTSQuerySessionInformation(ptrOpenedServer, (int) active_session, WTS_INFO_CLASS.WTSDomainName, str, out returned))
            {
                ClientInfo.WTSDomainName = str;
            }

            //Skip client name and client address if this is a console session
            if (ClientInfo.WTSStationName != "Console")
            {
                if (WTSQuerySessionInformation(ptrOpenedServer, (int) active_session, WTS_INFO_CLASS.WTSClientName, str, out returned))
                {
                    ClientInfo.WTSClientName = str;
                }

                //Get client IP address
                IntPtr addr;
                if (WTSQuerySessionInformation(ptrOpenedServer, (int) active_session, WTS_INFO_CLASS.WTSClientAddress, addr, out returned))
                {
                    WTS_CLIENT_ADDRESS obj = new WTS_CLIENT_ADDRESS();
                    obj = (WTS_CLIENT_ADDRESS)Marshal.PtrToStructure(addr, obj.GetType());
                    ClientInfo.Address[2] = obj.Address[2];
                    ClientInfo.Address[3] = obj.Address[3];
                    ClientInfo.Address[4] = obj.Address[4];
                    ClientInfo.Address[5] = obj.Address[5];
                }
            }
            WTSCloseServer(ptrOpenedServer);
            return true;
        }

        private string GetConnectionState(WTS_CONNECTSTATE_CLASS State)
        {
            string RetVal;
            switch (State)
            {
                case WTS_CONNECTSTATE_CLASS.WTSActive:
                    RetVal = "Active";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSConnected:
                    RetVal = "Connected";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSConnectQuery:
                    RetVal = "Query";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSDisconnected:
                    RetVal = "Disconnected";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSDown:
                    RetVal = "Down";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSIdle:
                    RetVal = "Idle";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSInit:
                    RetVal = "Initializing.";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSListen:
                    RetVal = "Listen";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSReset:
                    RetVal = "reset";
                    break;
                case WTS_CONNECTSTATE_CLASS.WTSShadow:
                    RetVal = "Shadowing";
                    break;
                default:
                    RetVal = "Unknown connect state";
                    break;
            }
            return RetVal;
        }

        //Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        //    Dim serverName As String
        //    Dim clientInfo As New WTS_CLIENT_INFO
        //    ReDim clientInfo.Address(20)
        //    serverName = ""
        //    'Server Name can be name of choice or name of server on which this application is running
        //    If GetSessions(serverName, clientInfo) = True Then
        //        Dim str As String
        //        str = "User Name: " & clientInfo.WTSUserName
        //        str &= vbNewLine & "Station Name: " & clientInfo.WTSStationName
        //        str &= vbNewLine & "Domain Name: " & clientInfo.WTSDomainName
        //        If clientInfo.WTSStationName <> "Console" Then
        //            str &= vbNewLine & "Client Name: " & clientInfo.WTSClientName
        //            str &= vbNewLine & "Client IP: " & clientInfo.Address(2) & "." & clientInfo.Address(3) & "." & clientInfo.Address(4) & "." & clientInfo.Address(5)
        //        End If
        //        MessageBox.Show(str)
        //    End If
        //End Sub

    }
}
