﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Logger
{
    /// <summary>
    /// The GlobalConstants class defines a set of publicly available constants.  All values are pubicly 
    /// available, so there is no need to instantiate an object of this type; therefore, no constructors 
    /// are accessible.
    /// </summary>
    public partial class GlobalConstants
    {
        /// <summary>
        /// Name of the application
        /// </summary>
        public const string AppName = "EntLibLoggerHelper";

        /// <summary>
        /// Name of the application
        /// </summary>
        public const string AppVersion = "EntLibLoggerHelper v1.0.0";
    }
}

