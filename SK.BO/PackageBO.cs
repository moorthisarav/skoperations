﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Logger;
using MongoDB.Bson;
using MongoDB.Driver;
using SK.DAO;
using SK.Library;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;

namespace SK.BO
{
    public class PackageBO
    {
        #region Public Methods

        public void UploadImages(long packageId)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(UploadImages), packageId);
        }        

        public void PrintLabels(string folioId, string userId)
        {
            List<string> param = new List<string>();
            param.Add(folioId);
            param.Add(userId);

            ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessPrinting), param);
        }

        #endregion

        #region private methods

        #region Printing labels

        private void ProcessPrinting(object param)
        {
            List<string>  paramList = (List<string> )param;
            CreatePackages(paramList[0], paramList[1]);
        }

        private void CreatePackages(string folioId, string userId)
        {
            try
            {
                MongoClient client = new MongoClient(DBConfig.DBConnectionString);
                MongoServer mongoServer = client.GetServer();
                mongoServer.Connect();

                MongoDatabase database = mongoServer.GetDatabase(DBConfig.DBName);

                using (mongoServer.RequestStart(database))
                {
                    //need to check folio paid expiration
                    
                    PackageDAO packageDAO = new PackageDAO();
                    CounterDAO counterDAO = new CounterDAO();
                    DateTime now = new DateTime();

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime packageDate = now.AddMonths(i);

                        Package package = new Package();
                        package.Id = counterDAO.GetNextCounterId("packageid", database);
                        package.Folio_id = new ObjectId(folioId);
                        package.Month = packageDate.Month;
                        package.Year = packageDate.Year;
                        package.CreatedOn = now;
                        package.UpdatedOn = now;
                        package.Status = (int)ProcessStatus.Created;

                        package.HistoryList = new List<PackageHistory>();
                        PackageHistory history = new PackageHistory();
                        history.StartedOn = now;
                        history.Status = package.Status;

                        package.HistoryList.Add(history);
                        packageDAO.Insert(database, package);
                    }
                }
                mongoServer.Disconnect();

            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("CreatePackages", exp.Message, exp, null);
            }
        }

        private void PrintAllLabels(string folioId, string userId)
        {
            try
            {
                
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("PrintLabels", exp.Message, exp, null);
            }
        }

        #endregion

        #region Upload Image

        private void UploadImages(object pkgeId)
        {
            try
            {
                long packageId = Convert.ToInt64(pkgeId);
                string activePath = Path.Combine(CommonConfig.ImageBasePath, "Active");
                string packageFolderPath = Path.Combine(activePath, packageId.ToString());

                if (Directory.Exists(packageFolderPath))
                {
                    LoggerHelper.LogTrace("UploadImages", string.Format("image upload started for {0}", packageId));

                    string[] files = Directory.GetFiles(packageFolderPath);

                    //if (files != null && files.Length > 0)
                    //{
                    UploadImageToDB(files, packageId);
                    Directory.Delete(packageFolderPath);
                    //}
                    //else
                    //  LoggerHelper.LogError("UploadImages", string.Format("No files in folder {0}", packageId));


                    LoggerHelper.LogTrace("UploadImages", string.Format("image upload completed for {0}", packageId));
                }
                else
                    LoggerHelper.LogError("UploadImages", string.Format("No package folder found {0}", packageId));
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("UploadImages", exp.Message, exp, null);
            }
        }

        private void UploadImageToDB(string[] files, long packageId)
        {
            MongoClient client = new MongoClient(DBConfig.DBConnectionString);
            MongoServer mongoServer = client.GetServer();
            mongoServer.Connect();

            MongoDatabase database = mongoServer.GetDatabase(DBConfig.DBName);

            using (mongoServer.RequestStart(database))
            {
                PackageDAO packageDAO = new PackageDAO();
                Package package = packageDAO.GetPackage(packageId, database);

                int errorCount = 0;


                if (files != null && files.Length > 0)
                {
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client())
                    {
                        CheckAndCreateBucket(s3Client, package.Folio_id.ToString());
                        
                        foreach (string file in files)
                        {
                            try
                            {
                                LoggerHelper.LogTrace("UploadImage", string.Format("start image upload {0} {1}", packageId, file));
                                UploadImage(database, s3Client, package, file);
                                LoggerHelper.LogTrace("UploadImage", string.Format("image upload completed {0} {1}", packageId, file));
                            }
                            catch (Exception exp)
                            {
                                LoggerHelper.LogError("UploadImage", exp.Message, exp, null);
                                MoveFileToErrorFolder(packageId, file);
                                errorCount++;
                            }
                        }
                    }
                }

                if (errorCount > 0)
                {
                    package.Status = (int)ProcessStatus.UploadError;
                    PackageHistory history = new PackageHistory();
                    history.Status = package.Status;

                    package.HistoryList.Add(history);
                }
                else
                    package.Status = (int)ProcessStatus.PackDigitize;

                package.HistoryList.Last().CompletedOn = DateTime.Now;

                packageDAO.Update(database, package);
            }

            mongoServer.Disconnect();
        }

        private void UploadImage(MongoDatabase database, IAmazonS3 s3Client, Package package, string fileName)
        {
            FileInfo fileinfo = new FileInfo(fileName);

            if (package != null)
            {
                FolioImage folioImage = new FolioImage();
                folioImage.Folio_id = package.Folio_id;
                folioImage.Year = package.Year;
                folioImage.Month = package.Month;
                folioImage.Day = DateTime.Now.Day;
                folioImage.Region = CommonConfig.AWSRegion;

                folioImage.Name = fileinfo.Name;

                Bitmap result = new Bitmap(fileName);
                string fullname = Guid.NewGuid().ToString() + ".JPG";
                string filePath = GetTempFilePath(fullname);
                result.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                folioImage.Width = result.Width;
                folioImage.Height = result.Height;
                folioImage.Type = "image/JPG";
                
                string sFolioId = folioImage.Folio_id.ToString();

                //save fullsize image
                UploadedImageAWS(s3Client, sFolioId, fullname, filePath);
                folioImage.Fullsize_image_id = fullname;

                //save web full 600px
                folioImage.Web_full_image_id = GetUploadedImage(s3Client, result, sFolioId, folioImage.Width, folioImage.Height, 600);

                //save web thumbnail 300px
                folioImage.Web_image_id = GetUploadedImage(s3Client, result, sFolioId, folioImage.Width, folioImage.Height, 300);

                //save icon 64px
                folioImage.Icon_image_id = GetUploadedImage(s3Client, result, sFolioId, folioImage.Width, folioImage.Height, 64);

                var collection = database.GetCollection<FolioImage>("folioimage");
                collection.Insert(folioImage);

                result.Dispose();

                MoveFileToArchiveFolder(package.Id, fileName);
            }
        }

        private void CheckAndCreateBucket(IAmazonS3 s3Client, string folioId)
        { 
           ListBucketsResponse response =  s3Client.ListBuckets();

            bool isBucketExists = false;

           if( response != null && response.Buckets != null && response.Buckets.Count > 0)
           {
               if(response.Buckets.Count(n => n.BucketName == folioId) > 0)
                   isBucketExists = true;
           }

           if (!isBucketExists)
           {
               PutBucketRequest bucketRequest = new PutBucketRequest();
               bucketRequest.BucketName = folioId;
               bucketRequest.CannedACL = S3CannedACL.PublicRead;
               bucketRequest.BucketRegionName = CommonConfig.AWSRegion;
               PutBucketResponse bucketresponse =  s3Client.PutBucket(bucketRequest);
           }

        }

        private void UploadedImageAWS(IAmazonS3 s3Client, string folioId, string fullName, string filePath)
        {
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = folioId;
            request.Key = fullName;
            request.ContentType = "image/jpeg";
            request.CannedACL = S3CannedACL.PublicRead;
            request.FilePath = filePath;

            PutObjectResponse response = s3Client.PutObject(request);
        }

        private string GetUploadedImage(IAmazonS3 s3Client, Bitmap result, string folioId, int originalWidth, int originalHeight, int targetWidth)
        {
            int percent = (targetWidth * 100) / originalWidth;
            int newWidth = (originalWidth * percent) / 100;
            int newHeight = (originalHeight * percent) / 100;

            Bitmap resized = null;
            //save web full 600px
            resized = ResizeImage(result, newWidth, newHeight);
            string fullName = Guid.NewGuid().ToString() + ".JPG";
            string filePath = GetTempFilePath(fullName);
            resized.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);

            if (resized != null)
                resized.Dispose();

            UploadedImageAWS(s3Client, folioId, fullName, filePath);

            return fullName;
        }
     
        private ObjectId GetUploadedImage(MongoDatabase database, Bitmap result, int originalWidth, int originalHeight, string filename, int targetWidth)
        {
            int percent = (targetWidth * 100) / originalWidth;
            int newWidth = (originalWidth * percent) / 100;
            int newHeight = (originalHeight * percent) / 100;

            Bitmap resized = null;
            //save web full 600px
            resized = ResizeImage(result, newWidth, newHeight);
            string fullsize = Guid.NewGuid().ToString() + ".JPG";
            string filePath = GetTempFilePath(fullsize);
            resized.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);

            var gridFsInfo = database.GridFS.Upload(filePath, filename);

            if (resized != null)
                resized.Dispose();

            return gridFsInfo.Id.AsObjectId;
        }

        private string GetTempFilePath(string filename)
        {
            string tempPath = Path.Combine(CommonConfig.ImageBasePath, "temp");

            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);

            tempPath = Path.Combine(tempPath, filename);

            return tempPath;
        }

        private void MoveFileToArchiveFolder(long packageId, string file)
        {
            MoveFileTo(packageId, file, "Archive");
        }

        private void MoveFileToErrorFolder(long packageId, string file)
        {
            MoveFileTo(packageId, file, "Error");
        }

        private void MoveFileTo(long packageId, string file, string folder)
        {
            string filePath = Path.Combine(CommonConfig.ImageBasePath, folder);

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            string packageFolderPath = Path.Combine(filePath, packageId.ToString());

            if (!Directory.Exists(packageFolderPath))
                Directory.CreateDirectory(packageFolderPath);

            string fileName = System.IO.Path.GetFileName(file);
            string destFile = System.IO.Path.Combine(packageFolderPath, fileName);

            File.Move(file, destFile);
        }

        private Bitmap ResizeImage(Bitmap b, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 
                    new Rectangle(0,0,width,height),
                    new Rectangle(0, 0, b.Width, b.Height),
                    GraphicsUnit.Pixel
                    );
                //g.DrawImage(b, 0, 0, height, height);
            return result;
        }

        #endregion

        #endregion
    }
}
