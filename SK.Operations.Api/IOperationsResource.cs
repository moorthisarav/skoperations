﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SK.Operations.Api
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IOperationsResource
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "getpackagefolder/{packageId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetPackageFolder(string packageId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "uploadimages/{packageId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UploadImages(string packageId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "print/{folioId}/{userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void PrintLabels(string folioId, string userId);

        // TODO: Add your service operations here
    }
}
