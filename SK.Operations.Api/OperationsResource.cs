﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Common.Logger;
using SK.BO;
using SK.Library;

namespace SK.Operations.Api
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class OperationsResource : IOperationsResource
    {
        public string GetPackageFolder(string packageId)
        {
            try
            {
                string activePath = Path.Combine(CommonConfig.ImageBasePath, "Active");

                if (!Directory.Exists(activePath))
                    Directory.CreateDirectory(activePath);

                string packageFolderPath = Path.Combine(activePath, packageId);

                if (!Directory.Exists(packageFolderPath))
                    Directory.CreateDirectory(packageFolderPath);

                return packageFolderPath;
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("CreatePackageFolder", exp.Message, exp, null);
                return "";
            }
        }

        public void UploadImages(string packageId)
        {
            try
            {
                PackageBO packageBO = new PackageBO();
                packageBO.UploadImages(Convert.ToInt64( packageId ));
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("UploadImages", exp.Message, exp, null);
            }
        }

        public void PrintLabels(string folioId, string userId)
        {
            try
            {
                PackageBO packageBO = new PackageBO();
                packageBO.PrintLabels(folioId, userId);
            }
            catch (Exception exp)
            {
                LoggerHelper.LogError("UploadImages", exp.Message, exp, null);
            }
        }
    }
}
