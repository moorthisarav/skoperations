﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.DAO
{
    public class DBConfig
    {
        private static string dbConnectionString = null;
        public static string DBConnectionString
        {
            get
            {
                if (dbConnectionString == null)
                {
                    dbConnectionString = ConfigurationManager.ConnectionStrings["sk_mongodb"].ConnectionString;
                    return dbConnectionString;
                }
                else
                    return dbConnectionString;
            }
        }

        private static string dbname = "schoolkeeps";
        public static string DBName
        {
            get
            {
                return dbname;
            }
        }
    }
}
