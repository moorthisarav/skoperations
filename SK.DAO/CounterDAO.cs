﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logger;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using SK.Library;

namespace SK.DAO
{
    public class CounterDAO
    {
        private MongoServer mongoServer = null;
        private bool disposed = false;
        private string collectionName = "counter";

        public CounterDAO()
        { 
        }

        public void Insert(Counter counter)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection();
                collection.Insert(counter);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Insert(MongoDatabase database, Counter counter)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection(database);
                collection.Insert(counter);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Update(Counter counter)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection();
                collection.Save(counter);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Update(MongoDatabase database, Counter counter)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection(database);
                collection.Save(counter);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public Counter GetCounter(string id)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection();
                return collection.FindOne(Query.EQ("_id", id));
            }
            catch (MongoConnectionException exp)
            {
                LoggerHelper.LogError("GetCounter", exp.Message, exp, null);
                return new Counter();
            }
        }

        public Counter GetCounter(string id, MongoDatabase database)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection(database);
                return collection.FindOne(Query.EQ("_id", id));
            }
            catch (MongoConnectionException exp)
            {
                LoggerHelper.LogError("GetCounter", exp.Message, exp, null);
                return new Counter();
            }
        }

        public long GetNextCounterId(string id, MongoDatabase database)
        {
            try
            {
                MongoCollection<Counter> collection = GetCollection(database);
                Counter counter = collection.FindOne(Query.EQ("_id", id));
                counter.Seq = counter.Seq + 1;
                Update(database, counter);
                return counter.Seq;

            }
            catch (MongoConnectionException exp)
            {
                LoggerHelper.LogError("GetCounter", exp.Message, exp, null);
                return -1;
            }
        }        

        private MongoCollection<Counter> GetCollection(MongoDatabase database)
        {
            MongoCollection<Counter> CounterCollection = database.GetCollection<Counter>(collectionName);
            return CounterCollection;
        }

        private MongoCollection<Counter> GetCollection()
        {

            if (mongoServer == null)
            {
                MongoClient client = new MongoClient(DBConfig.DBConnectionString);
                mongoServer = client.GetServer(); 
            }

            MongoDatabase database = mongoServer.GetDatabase(DBConfig.DBName);
            return GetCollection(database);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (mongoServer != null)
                    {
                        this.mongoServer.Disconnect();
                    }
                }
            }


            this.disposed = true;
        }
    }
}
