﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logger;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using SK.Library;

namespace SK.DAO
{
    public class PackageDAO : IDisposable
    {
        private MongoServer mongoServer = null;
        private bool disposed = false;
        private string collectionName = "package";
        
        public PackageDAO()
        { 
        }

        public void Insert(Package package)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection();
                collection.Insert(package);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Insert(MongoDatabase database, Package package)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection(database);
                collection.Insert(package);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Update(Package package)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection();
                collection.Save(package);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public void Update(MongoDatabase database, Package package)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection(database);
                collection.Save(package);
            }
            catch (MongoConnectionException exp)
            {
                throw new Exception("Error updating " + exp.Message);
            }
        }

        public Package GetPackage(long id)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection();
                return collection.FindOne(Query.EQ("_id", id));
            }
            catch (MongoConnectionException exp)
            {
                LoggerHelper.LogError("GetPackage", exp.Message, exp, null);
                return new Package();
            }
        }

        public Package GetPackage(long id, MongoDatabase database)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection(database);
                return collection.FindOne(Query.EQ("_id", id));
            }
            catch (MongoConnectionException exp)
            {
                LoggerHelper.LogError("GetPackage", exp.Message, exp, null);
                return new Package();
            }
        }

        public long GetPackageCount(ObjectId folioId)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection();
                return collection.Count(Query.EQ("_folio_id", folioId));
            }
            catch (MongoConnectionException)
            {
                return -1;
            }
        }

        public List<Package> GetAll()
        {
            try
            {
                MongoCollection<Package> collection = GetCollection();
                return collection.FindAll().ToList<Package>();
            }
            catch (MongoConnectionException)
            {
                return new List<Package>();
            }
        }

        public List<Package> GetAll(MongoDatabase database)
        {
            try
            {
                MongoCollection<Package> collection = GetCollection(database);
                return collection.FindAll().ToList<Package>();
            }
            catch (MongoConnectionException)
            {
                return new List<Package>();
            }
        }

        private MongoCollection<Package> GetCollection(MongoDatabase database)
        {
            MongoCollection<Package> PackageCollection = database.GetCollection<Package>(collectionName);
            return PackageCollection;
        }

        private MongoCollection<Package> GetCollection()
        {

            if (mongoServer == null)
            {
                MongoClient client = new MongoClient(DBConfig.DBConnectionString);
                mongoServer = client.GetServer(); 
            }

            MongoDatabase database = mongoServer.GetDatabase(DBConfig.DBName);
            return GetCollection(database);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (mongoServer != null)
                    {
                        this.mongoServer.Disconnect();
                    }
                }
            }


            this.disposed = true;
        }
    }
}
