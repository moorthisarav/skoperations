﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace SK.Library
{
    public class Storage
    {
        private int bin_id;

        [BsonElement("bin_id")]
        public int Bin_id
        {
            get { return bin_id; }
            set { bin_id = value; }
        }

        private int slot_id;

        [BsonElement("slot_id")]
        public int sSot_id
        {
            get { return slot_id; }
            set { slot_id = value; }
        }
    }
}
