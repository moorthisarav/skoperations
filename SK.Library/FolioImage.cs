﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace SK.Library
{
    public class FolioImage
    {
        public FolioImage()
        {
            likes = new List<ObjectId>();
            comments = new List<Comment>();
        }
        
        private string name;

        [BsonElement("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string type;

        [BsonElement("type")]
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        private string region;

        [BsonElement("region")]
        public string Region
        {
            get { return region; }
            set { region = value; }
        }

        private string contents;

        [BsonElement("contents")]
        public string Contents
        {
            get { return contents; }
            set { contents = value; }
        }

        private string description;

        [BsonElement("description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string category;

        [BsonElement("category")]
        public string Category
        {
            get { return category; }
            set { category = value; }
        }
        
        private ObjectId _id;

        [BsonId(IdGenerator = typeof(ObjectIdGenerator))]
        [BsonElement("_id")]
        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        private ObjectId folio_id;

        [BsonElement("folio_id")]
        public ObjectId Folio_id
        {
            get { return folio_id; }
            set { folio_id = value; }
        }

        private string fullsize_image_id;

        [BsonElement("fullsize_image_id")]
        public string Fullsize_image_id
        {
            get { return fullsize_image_id; }
            set { fullsize_image_id = value; }
        }

        private string web_full_image_id;

        [BsonElement("web_full_image_id")]
        public string Web_full_image_id
        {
            get { return web_full_image_id; }
            set { web_full_image_id = value; }
        }

        private string web_image_id;

        [BsonElement("web_image_id")]
        public string Web_image_id
        {
            get { return web_image_id; }
            set { web_image_id = value; }
        }

        private string icon_image_id;

        [BsonElement("icon_image_id")]
        public string Icon_image_id
        {
            get { return icon_image_id; }
            set { icon_image_id = value; }
        }

        private int day;

        [BsonElement("day")]
        public int Day
        {
            get { return day; }
            set { day = value; }
        }

        private int month;

        [BsonElement("month")]
        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        private int year;

        [BsonElement("year")]
        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        private int width;

        [BsonElement("width")]
        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        private int height;

        [BsonElement("height")]
        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        private List<ObjectId> likes;

        [BsonElement("likes")]
        public List<ObjectId> Likes
        {
            get { return likes; }
            set { likes = value; }
        }

        private List<Comment> comments;

        [BsonElement("comments")]
        public List<Comment> Comments
        {
            get { return comments; }
            set { comments = value; }
        }
    }
}
