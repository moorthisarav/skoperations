﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Library
{
    public class CommonConfig
    {
        private static string imageBasePath;

        public static string ImageBasePath
        {
            get 
            {
                if (string.IsNullOrEmpty(imageBasePath))
                {
                    CommonConfig.imageBasePath = ConfigurationManager.AppSettings["ImageBasePath"];
                }

                return CommonConfig.imageBasePath; 
            }
        }

        private static string baseAddress;

        public static string BaseAddress
        {
            get
            {
                if (string.IsNullOrEmpty(baseAddress))
                {
                    CommonConfig.baseAddress = ConfigurationManager.AppSettings["BaseAddress"];
                }

                return CommonConfig.baseAddress;
            }
        }

        private static string awsRegion;

        public static string AWSRegion
        {
            get
            {
                if (string.IsNullOrEmpty(awsRegion))
                {
                    CommonConfig.awsRegion = ConfigurationManager.AppSettings["AWSRegion"];
                }

                return CommonConfig.awsRegion;
            }
        }
    }
}
