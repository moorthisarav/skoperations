﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson;

namespace SK.Library
{
    public enum ProcessStatus
    {
        Created = 0,
        PrepareEnv = 1,
        Pack = 2,
        MailEnv = 3,
        UnPack = 4,
        Digitize = 5,
        VerifyDigitize = 6,
        UploadDigitized = 7,
        UploadError = 8,
        PackDigitize = 9,
        Storage = 10,
        STORED = 11
    }
    
    public class Package
    {
        public Package()
        {
            createdOn = DateTime.UtcNow;
            updatedOn = DateTime.UtcNow;
        }

        private long id;

        [BsonElement("_id")]
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private ObjectId folio_id;

        [BsonElement("folio_id")]
        public ObjectId Folio_id
        {
            get { return folio_id; }
            set { folio_id = value; }
        }

        private int month;

        [BsonElement("month")]
        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        private int year;

        [BsonElement("year")]
        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        private int status;

        [BsonElement("status")]
        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        private DateTime createdOn;

        [BsonElement("createdOn")]
        public DateTime CreatedOn
        {
            get {
                return createdOn.ToLocalTime();  
            }
            set { createdOn = value; }
        }

        private DateTime updatedOn;

        [BsonElement("updatedOn")]
        public DateTime UpdatedOn
        {
            get
            {
                return updatedOn.ToLocalTime();
            }
            set { updatedOn = value; }
        }

        private List<PackageHistory> historyList;

        [BsonElement("historyList")]
        public List<PackageHistory> HistoryList
        {
            get
            {
                return historyList;
            }
            set { historyList = value; }
        }

        private Storage storage;

        [BsonElement("storage")]
        public Storage Storage
        {
            get
            {
                return storage;
            }
            set { storage = value; }
        }
    }
}
