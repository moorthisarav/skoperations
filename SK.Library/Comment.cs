﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace SK.Library
{
    public class Comment
    {
        private string name;

        [BsonElement("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string profileImageUrl;

        [BsonElement("profileImageUrl")]
        public string ProfileImageUrl
        {
            get { return profileImageUrl; }
            set { profileImageUrl = value; }
        }

        private string summary;

        [BsonElement("summary")]
        public string Summary
        {
            get { return summary; }
            set { summary = value; }
        }
    }
}
