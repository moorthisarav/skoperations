﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace SK.Library
{
    public class PackageHistory
    {
        public PackageHistory()
        {
            StartedOn = DateTime.UtcNow;
            CompletedOn = DateTime.UtcNow;
        }
        
        private int status;

        [BsonElement("status")]
        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        private DateTime startedOn;

        [BsonElement("startedOn")]
        public DateTime StartedOn
        {
            get
            {
                return startedOn.ToLocalTime();
            }
            set { startedOn = value; }
        }

        private DateTime completedOn;

        [BsonElement("CompletedOn")]
        public DateTime CompletedOn
        {
            get
            {
                return completedOn.ToLocalTime();
            }
            set { completedOn = value; }
        }
    }
}
