﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace SK.Library
{
    public class Counter
    {
        private string id;

        [BsonElement("_id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private long seq;

        [BsonElement("seq")]
        public long Seq
        {
            get { return seq; }
            set { seq = value; }
        }
    }
}
